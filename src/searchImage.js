const axios = require("axios").default;
const DomParser = require('dom-parser');


async function getImage(item) {
    const url = `https://www.google.com.br/search?q=${item}&tbm=isch`;

    const response = await axios.get(url);

    const parser = new DomParser();

    const doc = parser.parseFromString(response.data, 'application/xhtml+xml');


    const firstImage = (doc.getElementsByTagName('img')[1]).getAttribute('src'); 
    
    return firstImage;
  
}

getImage('Pasta de Dente Colgate').then(console.log);


